// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ShooterCharacterAIController.generated.h"

/**
 * 
 */
UCLASS()
class SCIFISHOOTER_API AShooterCharacterAIController : public AAIController
{
	GENERATED_BODY()

private:

	virtual void Tick(float DeltaTime) override;
	
protected:

	virtual void BeginPlay() override;

public:

	bool IsDeadOrDetached() const;

	APawn* Player = nullptr;
	class AShooterCharacter* EnemyCharacter = nullptr;
	UPROPERTY(EditAnywhere) float RandomMovementRadius = 1000;
	UPROPERTY(EditAnywhere) class UBehaviorTree* BehaviorTree;
};
