// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_UpdatePlayerInfo.h"
#include "Kismet/GameplayStatics.h"
#include "ShooterCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "ShooterCharacterAIController.h"

UBTService_UpdatePlayerInfo::UBTService_UpdatePlayerInfo()
{
	NodeName = TEXT("Update Player Info");
}

void UBTService_UpdatePlayerInfo::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	auto Controller = OwnerComp.GetAIOwner();
	auto Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	auto EnemyCharacter = Cast<AShooterCharacter>(Controller->GetPawn());
	auto BB = OwnerComp.GetBlackboardComponent();
	
	if (Controller->LineOfSightTo(Player)) {
		BB->SetValueAsVector(PlayerLastLocation.SelectedKeyName, Player->GetActorLocation());
		BB->SetValueAsBool(PlayerIsVisible.SelectedKeyName, true);
		BB->SetValueAsBool(IsInvistigating.SelectedKeyName, true);
	} else {
		BB->ClearValue(PlayerIsVisible.SelectedKeyName);
	}
}
