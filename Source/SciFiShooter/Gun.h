// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

UCLASS()
class SCIFISHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();

	void PullTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool BulletTrace(FHitResult& Result, FVector& HitDirection);
	void HitBullet(const FHitResult& HitResult, const FVector& HitDirection);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere) USceneComponent* Root;

	UPROPERTY(VisibleAnywhere) USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere) UParticleSystem* MuzzleFlash = nullptr;
	UPROPERTY(EditAnywhere) UParticleSystem* ImpactEffect = nullptr;

	UPROPERTY(EditAnywhere) USoundBase* MuzzleSound = nullptr;
	UPROPERTY(EditAnywhere) USoundBase* ImpactSound = nullptr;

	UPROPERTY(EditAnywhere) float Range = 2000;
	UPROPERTY(EditAnywhere) float Damage = 10;
};
