// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Shoot.h"
#include "ShooterCharacterAIController.h"
#include "EnemyShooterCharacter.h"

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto Controller = OwnerComp.GetAIOwner();
	auto Enemy = Cast<AEnemyShooterCharacter>(Controller->GetPawn());
	Enemy->Shoot();
	return EBTNodeResult::Succeeded;
}
