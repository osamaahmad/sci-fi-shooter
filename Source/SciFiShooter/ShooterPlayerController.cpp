// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayerController.h"
#include "TimerManager.h"
#include "Blueprint/UserWidget.h"

void AShooterPlayerController::BeginPlay()
{
	if (CrosshairsClass) {
		Crosshairs = CreateWidget(this, CrosshairsClass);
		if (Crosshairs) {
			Crosshairs->AddToViewport();
		}
	}

	if (HealthBarClass) {
		HealthBar = CreateWidget(this, HealthBarClass);
		if (HealthBar) {
			HealthBar->AddToViewport();
		}
	}
}

void AShooterPlayerController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	// This is a quick hack. You should disable the input when losing
	// so that you can't shoot while you're dead, and win after losing.
	if (bGameAlreadyEnded)
		return;
	bGameAlreadyEnded = true;

	if (Crosshairs)
		Crosshairs->RemoveFromViewport();
	if (HealthBar)
		HealthBar->RemoveFromViewport();

	if (bIsWinner) {
		UUserWidget* WinScreen = CreateWidget(this, WinScreenClass);
		if (WinScreen) {
			WinScreen->AddToViewport();
		}
	} else {
		UUserWidget* LoseScreen = CreateWidget(this, LoseScreenClass);
		if (LoseScreen) {
			LoseScreen->AddToViewport();
		}
	}

	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}
