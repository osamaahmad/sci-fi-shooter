// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_UpdateRandomLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"


UBTService_UpdateRandomLocation::UBTService_UpdateRandomLocation()
{
	NodeName = TEXT("Update Random Location");
}

void UBTService_UpdateRandomLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	float Radius = OwnerComp.GetBlackboardComponent()->GetValueAsFloat(RadiusKey.SelectedKeyName);
	FVector OriginalLocation = OwnerComp.GetBlackboardComponent()->GetValueAsVector(OriginalLocationKey.SelectedKeyName);

	FNavLocation RandomReachableLocation;
	auto Nav = UNavigationSystemV1::GetCurrent(GetWorld());
	bool Found = Nav->GetRandomReachablePointInRadius(OriginalLocation, Radius, RandomReachableLocation);

	if (Found) {
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(TargetLocationKey.SelectedKeyName, RandomReachableLocation);
	}
}
