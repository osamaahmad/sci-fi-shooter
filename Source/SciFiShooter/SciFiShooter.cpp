// Copyright Epic Games, Inc. All Rights Reserved.

#include "SciFiShooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SciFiShooter, "SciFiShooter" );
