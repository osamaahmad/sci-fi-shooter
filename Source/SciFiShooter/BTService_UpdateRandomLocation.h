// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_UpdateRandomLocation.generated.h"

/**
 * 
 */
UCLASS()
class SCIFISHOOTER_API UBTService_UpdateRandomLocation : public UBTService
{
	GENERATED_BODY()

public:

	UBTService_UpdateRandomLocation();

	void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector RadiusKey;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector OriginalLocationKey;
	
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector TargetLocationKey;
};
