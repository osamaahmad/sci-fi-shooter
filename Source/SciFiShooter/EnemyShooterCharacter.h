// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterCharacter.h"
#include "EnemyShooterCharacter.generated.h"

/**
 * 
 */
UCLASS()
class SCIFISHOOTER_API AEnemyShooterCharacter : public AShooterCharacter
{
	GENERATED_BODY()
	
public:

	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};
