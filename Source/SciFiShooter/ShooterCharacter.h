// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

class AGun;

UCLASS()
class SCIFISHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void Shoot();
	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable) bool IsDead() const;
	UFUNCTION(BlueprintCallable) float GetHealthPercentage() const;

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);

	// Controller stuff
	UPROPERTY(EditAnywhere) float RotationRate = 70;
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);

	UPROPERTY(EditDefaultsOnly) TSubclassOf<AGun> GunClass;
	UPROPERTY() AGun* Gun;

	UPROPERTY(EditAnywhere) float MaxHealth = 100;
	UPROPERTY(VisibleAnywhere) float Health;
};
