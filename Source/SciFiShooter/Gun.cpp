// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Kismet/GamePlayStatics.h"
#include <SciFiShooter/ShooterCharacter.h>


// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}

void AGun::PullTrigger()
{
	if (MuzzleFlash)
		UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	if (MuzzleSound)
		UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

	FHitResult Result;
	FVector HitDirection;
	bool Hit = BulletTrace(Result, HitDirection);

	if (Hit) {
		HitBullet(Result, HitDirection);
	}
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
	
}

bool AGun::BulletTrace(FHitResult& Result, FVector& HitDirection)
{
	APawn* owner = Cast<APawn>(GetOwner());
	if (!owner) return false;
	AController* Controller = owner->GetController();

	FVector Start;
	FRotator Rotation;
	Controller->GetPlayerViewPoint(Start, Rotation);
	FVector End = Start + Rotation.Vector() * Range;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	bool Hit = GetWorld()->LineTraceSingleByChannel(Result, Start, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
	HitDirection = -Rotation.Vector();

	return Hit;
}

void AGun::HitBullet(const FHitResult& HitResult, const FVector& HitDirection)
{
	APawn* owner = Cast<APawn>(GetOwner());
	if (!owner) return;
	AController* Controller = owner->GetController();

	AActor* HitActor = HitResult.GetActor();
	if (HitActor && HitActor != owner) 
	{
		FPointDamageEvent DamageEvent(Damage, HitResult, HitDirection, nullptr);
		HitActor->TakeDamage(Damage, DamageEvent, Controller, this);

		auto Casted = Cast<AShooterCharacter>(HitActor);
		if (Casted) {
			float ActorHealth = Casted->Health;
			UE_LOG(LogTemp, Warning, TEXT("Health: %f"), ActorHealth);
		}

		if (ImpactEffect)
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, HitResult.Location, HitDirection.Rotation());
		if (ImpactSound)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, HitResult.Location);
	}
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

