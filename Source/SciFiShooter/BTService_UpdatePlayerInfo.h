// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_UpdatePlayerInfo.generated.h"

/**
 * 
 */
UCLASS()
class SCIFISHOOTER_API UBTService_UpdatePlayerInfo : public UBTService
{
	GENERATED_BODY()
	
public:

	UBTService_UpdatePlayerInfo();

	void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector PlayerIsVisible;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector IsInvistigating;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector PlayerLastLocation;
};
