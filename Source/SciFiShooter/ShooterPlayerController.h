// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SCIFISHOOTER_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	void BeginPlay() override;

private:

	void GameHasEnded(class AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;

	FTimerHandle RestartTimer;

	UPROPERTY(EditAnywhere) float RestartDelay = 2;

	UPROPERTY(EditAnywhere) TSubclassOf<class UUserWidget> LoseScreenClass = nullptr;
	UPROPERTY(EditAnywhere) TSubclassOf<class UUserWidget> WinScreenClass = nullptr;

	UPROPERTY(EditAnywhere) TSubclassOf<class UUserWidget> CrosshairsClass = nullptr;
	UPROPERTY() UUserWidget* Crosshairs = nullptr;
	UPROPERTY(EditAnywhere) TSubclassOf<class UUserWidget> HealthBarClass = nullptr;
	UPROPERTY() UUserWidget* HealthBar = nullptr;

	bool bGameAlreadyEnded = false;
};
