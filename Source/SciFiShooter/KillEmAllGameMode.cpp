// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"
#include "ShooterPlayerController.h"
#include "EngineUtils.h"
#include "ShooterCharacterAIController.h"


void AKillEmAllGameMode::PawnKilled(APawn* Pawn)
{
	Super::PawnKilled(Pawn);
	
	if (Cast<AShooterPlayerController>(Pawn->GetController())) {
		// The main player is killed
		EndGame(false);
		return;
	}
	
	// TODO This might be optimized.
	for (AController* Controller : TActorRange<AController>(GetWorld())) {
		auto Casted = Cast<AShooterCharacterAIController>(Controller);
		if (Casted && !Casted->IsDeadOrDetached()) {
			return;
		}
	}

	EndGame(true);
}

void AKillEmAllGameMode::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld())) {
		// If the player wins, enemies lose, and vice versa.
		bool bIsWinner = bIsPlayerWinner == Controller->IsPlayerController();
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}
