// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacterAIController.h"
#include "Kismet/GameplayStatics.h"
#include "ShooterCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"


void AShooterCharacterAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (EnemyCharacter->IsDead()) {
		// If the value is not set, the enemy won't follow the player.
		GetBlackboardComponent()->ClearValue(TEXT("PlayerLastLocation"));
	} else if (LineOfSightTo(Player)) {
		GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLastLocation"), Player->GetActorLocation());
		GetBlackboardComponent()->SetValueAsBool(TEXT("PlayerIsVisible"), true);
		GetBlackboardComponent()->SetValueAsBool(TEXT("IsInvistigating"), true);
	} else {
		GetBlackboardComponent()->ClearValue(TEXT("PlayerIsVisible"));
	}
}

void AShooterCharacterAIController::BeginPlay()
{
	Super::BeginPlay();

	Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	EnemyCharacter = Cast<AShooterCharacter>(GetPawn());
	
	if (BehaviorTree) {
		RunBehaviorTree(BehaviorTree); // This has to be set first.
		auto BB = GetBlackboardComponent();
		BB->SetValueAsFloat(TEXT("RandomMovementRadius"), RandomMovementRadius);
		BB->SetValueAsVector(TEXT("OriginalLocation"), EnemyCharacter->GetActorLocation());
		BB->SetValueAsObject(TEXT("Player"), Player);
	}
}

bool AShooterCharacterAIController::IsDeadOrDetached() const
{
	auto ControlledCharacter = Cast<AShooterCharacter>(GetPawn());

	if (ControlledCharacter) {
		return ControlledCharacter->IsDead();
	}

	// Detached
	return true;
}

