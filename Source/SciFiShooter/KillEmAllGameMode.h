// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SciFiShooterGameModeBase.h"
#include "KillEmAllGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SCIFISHOOTER_API AKillEmAllGameMode : public ASciFiShooterGameModeBase
{
	GENERATED_BODY()
	
public:

	void PawnKilled(class APawn* Pawn) override;

	void EndGame(bool bIsPlayerWinner);
};
